#!/bin/bash

LOADS="empty digest fail2ban read_all_fields"
#LOADS="empty digest"
PATCH_SETS=("" "init_reader_nochainmap" "get_next_lazy" "get_next_lazy_prefetched" "convert_entry_lazy" "init_reader_nochainmap get_next_lazy" "init_reader_nochainmap get_next_lazy_prefetched" "init_reader_nochainmap convert_entry_lazy")
#PATCH_SETS=("init_reader_nochainmap get_next_lazy" "init_reader_nochainmap get_next_lazy_prefetched" "init_reader_nochainmap convert_entry_lazy")

OUTPUT_PREFIX="report"
OUTPUT_AGGREGATE="${OUTPUT_PREFIX}_aggregate.csv"

aggregate_report() {
	INPUT=$1
	LOAD=$2
	PATCHES=$3

	CYCLES=$(grep cycles "$INPUT" | cut -d, -f1)

	echo -n "${CYCLES}," >> "$OUTPUT_AGGREGATE"
}

run_benchmark() {
	OUTPUT=$1
	LOAD=$2
	PATCHES=$3

	CURRENT_OUTPUT="${OUTPUT_PREFIX}_perf.txt"

	echo "$3" >> "$OUTPUT"
	env LANG=C perf stat -r5 -x"," --output "$CURRENT_OUTPUT" python3.7 benchmark_journal.py --load "$LOAD" --patches $PATCHES
	aggregate_report "$CURRENT_OUTPUT" "$LOAD" "$PATCHES"
	cat "$CURRENT_OUTPUT" >> "$OUTPUT"
	echo "" >> "$OUTPUT"
	echo "" >> "$OUTPUT"
}

truncate -s0 "${OUTPUT_AGGREGATE}"
for PATCHES in "${PATCH_SETS[@]}"; do
	# Insert an empty column for loads
	echo -n ",${PATCHES}" >> "${OUTPUT_AGGREGATE}"
done

for LOAD in $LOADS; do
	echo "Load: $LOAD"
	OUTPUT="${OUTPUT_PREFIX}_${LOAD}.csv"
	truncate -s0 "$OUTPUT"

	echo -en "\n${LOAD}," >> "$OUTPUT_AGGREGATE"
	for PATCHES in "${PATCH_SETS[@]}"; do
		run_benchmark "$OUTPUT" "$LOAD" "$PATCHES"
	done
done
