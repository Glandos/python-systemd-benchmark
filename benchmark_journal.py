#!/usr/bin/env python3.7

import sys
import time
import abc
import argparse

from dataclasses import dataclass, field
from typing import Set

from systemd import journal

import monkey_patches


class JournalLoad(abc.ABC):

    @classmethod
    def load_name(cls):
        name = cls.__name__
        if name.endswith('Load'):
            name = name[:-4]
        name = ''.join(c if c.islower() or not c.isalpha() else f'{"_" if i > 0 else ""}{c.lower()}' for (i, c) in enumerate(name))
        return name

    @abc.abstractmethod
    def digest(self, entry):
        pass

    @abc.abstractmethod
    def report(self):
        pass


class EmptyLoad(JournalLoad):

    def digest(self, entry):
        pass

    def report(self):
        return 'End of empty load'


@dataclass
class ReadAllFieldsLoad(JournalLoad):

    fields: int = 0

    def digest(self, entry):
        for key, value in entry.items():
            self.fields += 1

    def report(self):
        return f'Read all journal fields with {self.fields} fields'


@dataclass
class DigestLoad(JournalLoad):

    count: int = 0
    units: Set[str] = field(default_factory=set)
    total_message_length: int = 0

    def digest(self, entry):
        self.count += 1
        self.units.add(entry.get('_SYSTEMD_UNIT'))
        self.total_message_length += len(entry['MESSAGE'])

    def report(self):
        return f'Read {self.count} messages from {len(self.units)} units with total size of {self.total_message_length}'


class Fail2banLoad(JournalLoad):
    
    def uni_decode(self, x, enc='UTF-8', errors='strict'):
        try:
            if isinstance(x, bytes):
                return x.decode(enc, errors)
            return x
        except (UnicodeDecodeError, UnicodeEncodeError): # pragma: no cover - unsure if reachable
            if errors != 'strict': 
                raise
        return x.decode(enc, 'replace')

    def digest(self, logentry):
        # Be sure, all argument of line tuple should have the same type:
        enc = 'UTF-8'
        logelements = []
        v = logentry.get('_HOSTNAME')
        if v:
            logelements.append(self.uni_decode(v, enc))
        v = logentry.get('SYSLOG_IDENTIFIER')
        if not v:
            v = logentry.get('_COMM')
        if v:
            logelements.append(self.uni_decode(v, enc))
            v = logentry.get('SYSLOG_PID')
            if not v:
                v = logentry.get('_PID')
            if v:
                logelements[-1] += ("[%i]" % v)
            logelements[-1] += ":"
            if logelements[-1] == "kernel:":
                if '_SOURCE_MONOTONIC_TIMESTAMP' in logentry:
                    monotonic = logentry.get('_SOURCE_MONOTONIC_TIMESTAMP')
                else:
                    monotonic = logentry.get('__MONOTONIC_TIMESTAMP')[0]
                logelements.append("[%12.6f]" % monotonic.total_seconds())
        msg = logentry.get('MESSAGE','')
        if isinstance(msg, list):
            logelements.append(" ".join(self.uni_decode(v, enc) for v in msg))
        else:
            logelements.append(self.uni_decode(msg, enc))

        logline = " ".join(logelements)

        date = logentry.get('_SOURCE_REALTIME_TIMESTAMP',
                logentry.get('__REALTIME_TIMESTAMP'))
        #logSys.log(5, "[%s] Read systemd journal entry: %s %s", self.jailName, date.isoformat(), logline)
        ## use the same type for 1st argument:
        return ((logline[:0], date.isoformat(), logline), time.mktime(date.timetuple()) + date.microsecond/1.0E6)

    def report(self):
        return 'End of Fail2Ban load'


LOADS = {cls.load_name(): cls for cls in locals().values() if isinstance(cls, type) and issubclass(cls, JournalLoad) and cls is not JournalLoad}
PATCHES = ['init_reader_nochainmap', 'get_next_lazy', 'get_next_lazy_prefetched', 'convert_entry_lazy', 'convert_value_nolist']


def read_journal(parser: argparse.ArgumentParser):
    reader = journal.Reader(path='./journals/')
    digest = LOADS[parser.load]()
    entry = reader.get_next()
    while True:
        if not entry:
            break
        digest.digest(entry)
        entry = reader.get_next()

    return digest


def apply_patches(names):
    journal.Reader._convert_value = monkey_patches.convert_value_dist
    journal.Reader._convert_entry = monkey_patches.convert_entry_using_convert_value
    for name in names:
        if name == 'init_reader_nochainmap':
            journal.Reader.__init__ = monkey_patches.init_reader_nochainmap
        elif name == 'get_next_lazy':
            journal.Reader._get_lazy_value = monkey_patches._get_lazy_value
            journal.Reader.get_next = monkey_patches.get_next_lazy
        elif name == 'get_next_lazy_prefetched':
            journal.Reader._get_lazy_value = monkey_patches._get_lazy_value_prefetched
            journal.Reader.get_next = monkey_patches.get_next_lazy_prefetched
        elif name == 'convert_entry_lazy':
            journal.Reader._convert_entry = monkey_patches.convert_entry_lazy
        elif name == 'convert_value_nolist':
            journal.Reader._convert_value = monkey_patches.convert_value_nolist


def parse_cli(argv):
    parser = argparse.ArgumentParser(description='Benchmark for reading journald logs using Python')
    parser.add_argument('--load', choices=LOADS.keys(), default='empty')
    parser.add_argument('--patches', nargs='*', choices=PATCHES)

    return parser.parse_args(argv)


if __name__ == '__main__':
    parser = parse_cli(sys.argv[1:])

    if parser.patches:
        apply_patches(parser.patches)

    print(f'Using {parser.load} load with {", ".join(parser.patches) if parser.patches else "no patch"}: ', end='', flush=True)
    load = read_journal(parser)
    print(load.report())
