Benchmarks for systemd Python bindings
======================================

This is a small tool to perform some benchmark for reading
journal files with python bindings.

The tool is able to apply some monkey patches to show the improvements
that can be made.

Requirements
============

- Linux perf
- Python 3.7
- python-systemd (or python3-systemd)

And some journal files to be put inside `journals` directory.

Usage
=====

- Run the tool without any changes:
```
python3.7 benchmark_journal.py 
```

- Run the tool with some patches:
```
python3.7 benchmark_journal.py init_reader_nochainmap get_next_lazy convert_value_nolist
```

Available patches
=================

- `init_reader_nochainmap`: Do not use ChainMap when finding converters. ChainMap are really slow.
- `convert_value_nolist`: Do not test if value's entry is a list. It cannot happen looking at the [current C code](https://github.com/systemd/python-systemd/blob/5d3be8ccba5fe48b09e2a4e816f21c39038adfd0/systemd/_reader.c#L531).
- `get_next_lazy`: Instead of fetching all values right now, it just return a proxy object, that fetch and convert values on demand.
- `convert_entry_lazy`: All raw value are fetched, but only converted on demand. This is useless with `get_next_lazy`.
