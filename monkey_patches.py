#!/usr/bin/env python3

import collections
import itertools

from systemd.journal import Reader, DEFAULT_CONVERTERS
from systemd._reader import LOCAL_ONLY


def convert_value_dist(self, key, value):
    if isinstance(value, list):
        return [self._convert_field(key, val) for val in value]
    else:
        return self._convert_field(key, value)


convert_value_nolist = Reader._convert_field


def init_reader_nochainmap(self, flags=None, path=None, files=None, converters=None):
        if flags is None:
            if path is None and files is None:
                # This mimics journalctl behaviour of default to local journal only
                flags = LOCAL_ONLY
            else:
                flags = 0

        super(Reader, self).__init__(flags, path, files)
        self.converters = DEFAULT_CONVERTERS.copy()
        if converters is not None:
            self.converters.update(converters)


class LazyEntry(collections.abc.Mapping):
    def __init__(self, getter, all_getter):
        self.getter = getter
        self.all_getter = all_getter
        self.values = None

    def __getitem__(self, key):
        return self.getter(key)

    def __iter__(self):
        if not self.values:
            self.values = self.all_getter()
            self.values.update({
                '__CURSOR': None,
                '__MONOTONIC_TIMESTAMP': None,
                '__REALTIME_TIMESTAMP': None,
            })
        return iter(self.values)

    def __len__(self):
        if not self.values:
            self.values = self.all_getter()
            self.values.update({
                '__CURSOR': None,
                '__MONOTONIC_TIMESTAMP': None,
                '__REALTIME_TIMESTAMP': None,
            })
        return len(self.values)

    def __bool__(self):
        return True


def _get_lazy_value(self, key):
    if key == '__CURSOR':
        value = self._get_cursor()
    elif key == '__MONOTONIC_TIMESTAMP':
        value = self._get_monotonic()
    elif key == '__REALTIME_TIMESTAMP':
        value = self._get_realtime()
    else:
        value = super(Reader, self)._get(key)

    return self._convert_value(key, value)


class LazyEntryPrefetched(collections.abc.Mapping):
    
    CUSTOM_KEYS = ('__CURSOR', '__MONOTONIC_TIMESTAMP', '__REALTIME_TIMESTAMP')

    def __init__(self, getter, values):
        self.getter = getter
        self.values = values

    def __getitem__(self, key):
        return self.getter(key, self.values)

    def __iter__(self):
        return itertools.chain(iter(self.values), LazyEntryPrefetched.CUSTOM_KEYS)

    def __len__(self):
        return len(self.values) + len(LazyEntryPrefetched.CUSTOM_KEYS)

    def __bool__(self):
        return True


def _get_lazy_value_prefetched(self, key, prefetched_values):
    if key == '__CURSOR':
        value = self._get_cursor()
    elif key == '__MONOTONIC_TIMESTAMP':
        value = self._get_monotonic()
    elif key == '__REALTIME_TIMESTAMP':
        value = self._get_realtime()
    else:
        value = prefetched_values[key]

    return self._convert_value(key, value)


def get_next_lazy(self, skip=1):
    if super(Reader, self)._next(skip):
        return LazyEntry(self._get_lazy_value, self._get_all)

    return dict()


def get_next_lazy_prefetched(self, skip=1):
    if super(Reader, self)._next(skip):
        return LazyEntryPrefetched(self._get_lazy_value, self._get_all())

    return dict()


class LazyValueDict(collections.abc.MutableMapping):

    def __init__(self, convert_function, store):
        self.convert_function = convert_function
        self.store = store

    def __setitem__(self, key, value):
        self.store[key] = value

    def __getitem__(self, key):
        value = self.store[key]
        value = self.convert_function(key, value)
        return value

    def __delitem__(self, key):
        del self.store[key]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

def convert_entry_lazy(self, entry):
    result = LazyValueDict(self._convert_value, entry)
    return result


def convert_entry_using_convert_value(self, entry):
    """Convert entire journal entry utilising _convert_field."""
    result = {}
    for key, value in entry.items():
        result[key] = self._convert_value(key, value)
    return result
